import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import QtGraphicalEffects 1.15
import Miko.Asuki 1.0
import Miko.MenuBar 1.0

BlurredWindow {
    id: root
    width: Screen.width
    height: 40
    minimumWidth: Screen.width
    minimumHeight: 40
    maximumWidth: Screen.width
    maximumHeight: 40
    visible: true
    x: 0
    y: 0
    flags: Qt.FramelessWindowHint | Qt.WindowDoesNotAcceptFocus
    color: "transparent"

    Settings {
        id: settings
    }

    Rectangle {
        anchors.fill: parent
        color: TailwindColor.opacity(TailwindColor.slate[settings.darkMode ? 800 : 200], 0.75)

        RowLayout {
            anchors.fill: parent
            anchors.leftMargin: 16
            anchors.rightMargin: 16
            spacing: 8

            IconButton {
                text: "home"
                color: settings.darkMode ? TailwindColor.slate[50] : TailwindColor.slate[900]
                onClicked: homePopout.visible = true
            }

            HomePopout {
                id: homePopout
            }

            Item {
                Layout.fillWidth: true
            }

            IconLabel {
                text: "notifications_active"
                color: settings.darkMode ? TailwindColor.slate[50] : TailwindColor.slate[900]
            }

            IconLabel {
                text: "volume_up"
                color: settings.darkMode ? TailwindColor.slate[50] : TailwindColor.slate[900]
            }

            IconLabel {
                text: "battery_charging_full"
                color: settings.darkMode ? TailwindColor.slate[50] : TailwindColor.slate[900]
            }

            IconLabel {
                text: "bluetooth_connected"
                color: settings.darkMode ? TailwindColor.slate[50] : TailwindColor.slate[900]
            }

            IconLabel {
                text: "network_wifi_2_bar"
                color: settings.darkMode ? TailwindColor.slate[50] : TailwindColor.slate[900]
            }

            Label {
                Layout.leftMargin: 16
                Layout.alignment: Qt.AlignVCenter
                text: getDate()
                font.pixelSize: 16
                font.family: 'Inter'
                font.weight: Font.DemiBold
                horizontalAlignment: Text.AlignRight

                function getDate() {
                    return new Date().toLocaleTimeString(Qt.locale(), Locale.ShortFormat)
                }

                Timer {
                    interval: 500
                    running: true
                    repeat: true
                    onTriggered: parent.text = parent.getDate()
                }
            }
        }
    }

    // Border
    Rectangle {
        width: parent.width
        height: 1
        y: parent.height - 1
        color: TailwindColor.slate[settings.darkMode ? 600 : 500]
    }
}
