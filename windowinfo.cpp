#include "windowinfo.h"
#include <KWindowInfo>
#include <KWindowSystem>

WindowInfo::WindowInfo(QObject *parent)
    : QObject{parent}
{

    connect(KWindowSystem::self(), &KWindowSystem::activeWindowChanged, this, &WindowInfo::onActiveWindowChanged);
    connect(KWindowSystem::self(), static_cast<void (KWindowSystem::*)(WId id, NET::Properties properties, NET::Properties2 properties2)>(&KWindowSystem::windowChanged), this, &WindowInfo::onWindowChanged);
}

QString WindowInfo::windowName() {
    KWindowInfo info(m_activeID, NET::WMName);
    if (info.valid()) {
        return info.name();
    }

    return "";
}

QString WindowInfo::windowIconName() {
    KWindowInfo info(m_activeID, NET::WMName, NET::WM2WindowClass);
    if (info.valid()) {
        return QString(info.windowClassName());
    }

    return "";
}

void WindowInfo::onActiveWindowChanged(WId id) {
    m_activeID = id;
    emit activeWindowChanged();
}

void WindowInfo::onWindowChanged(WId id, NET::Properties properties, NET::Properties2 properties2) {
    (void) properties;
    (void) properties2;

    if (id == m_activeID) {
        emit activeWindowChanged();
    }
}
