import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import QtGraphicalEffects 1.15
import xyz.omame.h4x.h4xlib 1.0

Rectangle {
    id: root
    Layout.fillWidth: true
    property string wifiName: "placeholder"
    property bool secure: false
    property bool connected: false
    height: layout.implicitHeight + 16
    radius: 8
    color: settings.darkMode ? H4xColor.zinc._800 : H4xColor.zinc._50
    border.width: 1
    border.color: settings.darkMode ? H4xColor.zinc._700 : H4xColor.zinc._300

    H4xSettings {
        id: settings
    }

    RowLayout {
        id: layout
        spacing: 8
        anchors.fill: parent
        anchors.margins: 8

        Image {
            source: "qrc:/icons/wifi.svg"
            Layout.alignment: Qt.AlignVCenter

            layer.enabled: true
            layer.effect: ColorOverlay {
                color: settings.darkMode ? H4xColor.zinc._50 : H4xColor.zinc._900
            }
        }

        Label {
            text: root.wifiName
            Layout.alignment: Qt.AlignVCenter
            Layout.fillWidth: true
        }

        Image {
            visible: root.secure
            source: "qrc:/icons/lock-closed.svg"
            Layout.alignment: Qt.AlignVCenter

            layer.enabled: true
            layer.effect: ColorOverlay {
                color: settings.darkMode ? H4xColor.zinc._50 : H4xColor.zinc._900
            }
        }

        Image {
            visible: root.connected
            source: "qrc:/icons/check-circle.svg"
            Layout.alignment: Qt.AlignVCenter

            layer.enabled: true
            layer.effect: ColorOverlay {
                color: settings.darkMode ? H4xColor.zinc._50 : H4xColor.zinc._900
            }
        }
    }

    layer.enabled: true
    layer.effect: DropShadow {
        verticalOffset: 1
        radius: 2
        samples: radius * 2 + 1
        color: Qt.rgba(0, 0, 0, 0.25)
    }
}
