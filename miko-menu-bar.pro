QT += quick dbus KWindowSystem x11extras

SOURCES += \
        apptracker.cpp \
        main.cpp \
        windowinfo.cpp \
        network.cpp

HEADERS += \
    apptracker.h \
    windowinfo.h \
    network.h

RESOURCES += resources.qrc \
    resources.qrc

TRANSLATIONS += \
    translations/miko-menu-bar_en_US.ts
CONFIG += lrelease
CONFIG += embed_translations link_pkgconfig
PKGCONFIG += libnm
INCLUDEPATH += $$[QT_SYSROOT]/usr/include/KF5/NetworkManagerQt
LIBS += -lKF5NetworkManagerQt
LIBS += -lX11

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

target.path = /usr/bin
INSTALLS += target
