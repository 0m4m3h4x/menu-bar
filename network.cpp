#include "network.h"

#include <NetworkManagerQt/Manager>
#include <NetworkManagerQt/Device>
#include <NetworkManagerQt/WirelessNetwork>

Network::Network(QObject *parent)
    : QObject{parent}
{
    NetworkManager::Device::List ifaces = NetworkManager::networkInterfaces();
    for (NetworkManager::Device::Ptr &device : ifaces) {
        if (device->type() == NetworkManager::Device::Wifi) {
            m_wirelessDevice = qobject_cast<NetworkManager::WirelessDevice::Ptr>(device);
        }
    }

    connect(m_wirelessDevice.get(), &NetworkManager::WirelessDevice::networkAppeared, this, &Network::networksChanged);
    connect(m_wirelessDevice.get(), &NetworkManager::WirelessDevice::networkDisappeared, this, &Network::networksChanged);
}

QStringList Network::networks() {
    QStringList list;
    if (m_wirelessDevice == nullptr) return list;

    for (NetworkManager::WirelessNetwork::Ptr &network : m_wirelessDevice->networks()) {
        list << network->ssid();
    }

    return list;
}
