#ifndef WINDOWINFO_H
#define WINDOWINFO_H

#include "qwindowdefs.h"
#include <QObject>
#include <qqml.h>
#include <KWindowSystem>

class WindowInfo : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString windowName READ windowName NOTIFY activeWindowChanged);
    Q_PROPERTY(QString windowIconName READ windowIconName NOTIFY activeWindowChanged);
    QML_ELEMENT

public:
    explicit WindowInfo(QObject *parent = nullptr);
    QString windowName();
    QString windowIconName();

signals:
    void activeWindowChanged();

public slots:
    void onActiveWindowChanged(WId id);
    void onWindowChanged(WId id, NET::Properties properties, NET::Properties2 properties2);

private:
    WId m_activeID = KWindowSystem::activeWindow();
};

#endif // WINDOWINFO_H
