#ifndef APPTRACKER_H
#define APPTRACKER_H

#include <qqml.h>
#include <QObject>
#include <QSettings>
#include <QList>
#include <QVariant>

class AppTracker : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QList<QVariant> apps READ apps CONSTANT)
    QML_ELEMENT
public:
    explicit AppTracker(QObject *parent = nullptr);

    QList<QVariant> apps();

private:
    QSettings m_settings;
    QList<QVariant> m_apps;
};

#endif // APPTRACKER_H
