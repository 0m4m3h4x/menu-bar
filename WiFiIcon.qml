import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import QtQuick.Window 2.15
import QtGraphicalEffects 1.15
import xyz.omame.h4x.h4xlib 1.0
import Miko.MenuBar 1.0

Image {
    id: icon
    source: "qrc:/icons/wifi.svg"
    Layout.alignment: Qt.AlignVCenter

    property int windowY: 0

    layer.enabled: true
    layer.effect: ColorOverlay {
        color: settings.darkMode ? H4xColor.zinc._50 : H4xColor.zinc._900
    }

    H4xSettings {
        id: settings
    }

    MouseArea {
        anchors.fill: parent
        hoverEnabled: true
        onClicked: popup.open()
    }

    CustomPopup {
        id: popup
        targetX: Screen.width - width - 16
        y: icon.windowY + 32 + 16
        width: 288
        height: layout.implicitHeight + 32
        maximumHeight: 350
        color: "transparent"

        Rectangle {
            anchors.fill: parent
            radius: 16
            color: settings.darkMode ? H4xColor.zinc._900 : H4xColor.zinc._100
            border.width: 1
            border.color: settings.darkMode ? H4xColor.zinc._600 : H4xColor.zinc._300

            Network {
                id: network
            }

            ColumnLayout {
                id: layout
                anchors.fill: parent
                anchors.margins: 16
                spacing: 8

                Label {
                    text: "Networks"
                    font.pixelSize: 20
                    font.weight: Font.DemiBold
                }

                ListView {
                    id: view
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    implicitHeight: contentHeight
                    model: network.networks
                    spacing: 8
                    clip: true
                    delegate: WiFiItem {
                        width: ListView.view.width
                        wifiName: modelData
                    }
                }
            }
        }
    }
}
