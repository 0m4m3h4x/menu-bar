import QtQuick 2.15
import Miko.Asuki 1.0

IconLabel {
    id: button
    signal clicked

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        onClicked: button.clicked()
        hoverEnabled: true
    }
}
