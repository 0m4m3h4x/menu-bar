#ifndef NETWORK_H
#define NETWORK_H

#include "qqml.h"
#include <QObject>

#include <NetworkManagerQt/WirelessDevice>

class Network : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QStringList networks READ networks NOTIFY networksChanged)
    QML_ELEMENT
public:
    explicit Network(QObject *parent = nullptr);
    QStringList networks();

signals:
    void networksChanged();

private:
    NetworkManager::WirelessDevice::Ptr m_wirelessDevice = nullptr;
};

#endif // NETWORK_H
