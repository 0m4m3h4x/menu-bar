#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QFontDatabase>
#include <QLocale>
#include <QTranslator>
#include <QScreen>
#include <QQuickWindow>

#include <KWindowSystem>

#include "windowinfo.h"
#include "network.h"
#include "apptracker.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QTranslator translator;
    const QStringList uiLanguages = QLocale::system().uiLanguages();
    for (const QString &locale : uiLanguages) {
        const QString baseName = "miko-menu-bar_" + QLocale(locale).name();
        if (translator.load(":/i18n/" + baseName)) {
            app.installTranslator(&translator);
            break;
        }
    }

    QQmlApplicationEngine engine;

    qmlRegisterType<WindowInfo>("Miko.MenuBar", 1, 0, "WindowInfo");
    qmlRegisterType<Network>("Miko.MenuBar", 1, 0, "Network");
    qmlRegisterType<AppTracker>("Miko.MenuBar", 1, 0, "AppTracker");

    const QUrl url("qrc:/main.qml");
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    QQuickWindow *rootWindow = reinterpret_cast<QQuickWindow*>(engine.rootObjects().constFirst());
    KWindowSystem::setType(rootWindow->winId(), NET::WindowType::Dock);
    KWindowSystem::setState(rootWindow->winId(), NET::SkipPager | NET::SkipSwitcher | NET::SkipTaskbar);
    KWindowSystem::setOnDesktop(rootWindow->winId(), -1);
    KWindowSystem::setOnAllDesktops(rootWindow->winId(), true);
    KWindowSystem::setExtendedStrut(rootWindow->winId(),
                                    0, 0, 0,
                                    0, 0, 0,
                                    rootWindow->height(), 0, rootWindow->width(),
                                    0, 0, 0);

    return app.exec();
}
