import QtQuick 2.15
import QtQuick.Layouts 1.15
import Miko.Asuki 1.0
import Miko.MenuBar 1.0

ColumnLayout {
    id: layout
    anchors.fill: parent
    spacing: 24

    required property AppTracker tracker

    RowLayout {
        Layout.fillWidth: true

        Label {
            text: "Apps"
            font.pixelSize: 24
            font.weight: Font.DemiBold
        }

        Item { Layout.fillWidth: true }

        Button {
            icon.name: "apps"
            text: "Show all"
        }
    }

    GridLayout {
        columns: 4
        columnSpacing: 16
        rowSpacing: 16

        Repeater {
            model: tracker.apps.slice(0, 4 * 3)
            delegate: ColumnLayout {
                width: 128
                Layout.minimumWidth: 128
                spacing: 8
                Layout.alignment: Qt.AlignTop

                ThemedIcon {
                    Layout.alignment: Qt.AlignHCenter
                    width: 64
                    height: 64
                    name: modelData.icon
                }

                Label {
                    text: modelData.name
                    Layout.alignment: Qt.AlignHCenter
                    Layout.maximumWidth: 128
                    horizontalAlignment: Text.AlignHCenter
                    wrapMode: Text.WordWrap
                    maximumLineCount: 2
                    clip: true
                    elide: Text.ElideRight
                }
            }
        }
    }
}
