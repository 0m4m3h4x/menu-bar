#include "apptracker.h"
#include <QStandardPaths>
#include <QDirIterator>
#include <QMap>
#include <algorithm>

AppTracker::AppTracker(QObject *parent)
    : QObject{parent}, m_settings("Miko", "MenuBar", this)
{
    QStringList paths = QStandardPaths::standardLocations(QStandardPaths::ApplicationsLocation);
    QStringList extensions;
    extensions << "*.desktop";
    for (QString &path : paths) {
        QDirIterator it(path, extensions);
        while (it.hasNext()) {
            QString file = it.next();
            QSettings fileSettings(file, QSettings::IniFormat);
            fileSettings.beginGroup("Desktop Entry");
            if (fileSettings.value("Type").toString() == "Application" && !fileSettings.value("NoDisplay", false).toBool()) {
                QMap<QString, QVariant> map;
                map.insert("name", fileSettings.value("Name"));
                map.insert("icon", fileSettings.value("Icon"));
                // qDebug() << file << map.value("name").toString();
                m_apps.append(QVariant(map));
            }
        }
    }

    std::sort(m_apps.begin(), m_apps.end(), [](const QVariant &v1, const QVariant &v2) -> bool {
        auto map1 = v1.toMap();
        auto map2 = v2.toMap();
        return (map1.value("name").toString()) < (map2.value("name").toString());
    });
}

QList<QVariant> AppTracker::apps() {
    return m_apps;
}
