import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import Miko.Asuki 1.0
import Miko.MenuBar 1.0

BlurredWindow {
    radius: background.radius
    width: background.width
    height: background.height
    color: "transparent"
    flags: Qt.FramelessWindowHint | Qt.WindowStaysOnTopHint
    x: 16
    y: 40 + 16

    onWindowActiveChanged: {
        if (!active) {
            visible = false
        }
    }

    Settings {
        id: settings
    }

    AppTracker {
        id: appTracker
    }

    Rectangle {
        id: background
        width: stackView.currentItem.implicitWidth + 24 * 2
        height: stackView.currentItem.implicitHeight + 24 * 2
        color: TailwindColor.opacity(TailwindColor.slate[settings.darkMode ? 800 : 200], 0.75)
        border.width: 1
        border.color: TailwindColor.slate[settings.darkMode ? 600 : 400]
        radius: 16

        StackView {
            id: stackView
            anchors.fill: parent
            anchors.margins: 24
            initialItem: HomeIndex {
                tracker: appTracker
            }
        }
    }
}
